<?php

require_once("../../../vendor/autoload.php");
use App\Message\Message;
echo Message::message();



?>



<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Bootstrap Login Form Template</title>

    <!-- CSS -->
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:400,100,300,500">
    <link rel="stylesheet" href="../../../Resourse/assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../../Resourse/assets/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="../../../Resourse/assets/css/form-elements.css">
    <link rel="stylesheet" href="../../../Resourse/assets/css/style.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Favicon and touch icons -->
    <link rel="shortcut icon" href="../../../Resourse/assets/ico/favicon.png">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../../../Resourse/assets/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../../../Resourse/assets/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../../../Resourse/assets/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="../../../Resourse/assets/ico/apple-touch-icon-57-precomposed.png">

</head>

<body>

<!-- Top content -->
<div class="top-content">

    <div class="inner-bg">
        <div class="container">

            </div>
            <div class="row">
                <div class="col-sm-6 col-sm-offset-3 form-box">
                    <div class="form-top">
                        <div class="form-top-left">
                            <h3> Add Book Title</h3>
                            <p>Enter Book Title and Author Name </p>
                        </div>
                        <div class="form-top-right">
                            <i class="fa fa-book"></i>
                        </div>
                    </div>
                    <div class="form-bottom">
                        <form role="form" action="store.php" method="post" class="login-form">
                            <div class="form-group">
                                <label class="sr-only" for="book_title">book_title</label>
                                <input type="text" name="book_title" placeholder="Book Title" class="book_title form-control" id="book_title">
                            </div>
                            <div class="form-group">
                                <label class="sr-only" for="author_name">author_name</label>
                                <input type="text" name="author_name" placeholder="Author Name" class="form-author_name form-control" id="form-author_name">
                            </div>
                            <button type="submit" class="btn">Create</button>
                        </form>
                    </div>
                </div>
            </div>
            <div class="row">

                </div>
            </div>
        </div>
    </div>

</div>


<!-- Javascript -->
<script src="../../../Resourse/assets/js/jquery-1.11.1.min.js"></script>
<script src="../../../Resourse/assets/bootstrap/js/bootstrap.min.js"></script>
<script src="../../../Resourse/assets/js/jquery.backstretch.min.js"></script>
<script src="../../../Resourse/assets/js/scripts.js"></script>

<!--[if lt IE 10]>
<script src="../../../Resourse/assets/js/placeholder.js"></script>
<![endif]-->

</body>

</html>