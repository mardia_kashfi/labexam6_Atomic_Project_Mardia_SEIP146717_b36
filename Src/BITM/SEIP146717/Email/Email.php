<?php
namespace App\Email;

use App\Model\Database as DB;


class Email extends DB{
    public $id;
    public $user_name;
    public $user_email;

    public function __construct()
    {
        parent::__construct();
    }

    public function index(){
        echo "I'm inside the index method of Email Class"."<br>";
    }

}