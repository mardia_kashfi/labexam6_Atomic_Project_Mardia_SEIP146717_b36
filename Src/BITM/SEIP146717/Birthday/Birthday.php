<?php
namespace App\Birthday;

use App\Model\Database as DB;


class Birthday extends DB{
    public $id;
    public $user_name;
    public $user_birthday;

    public function __construct()
    {
        parent::__construct();
    }

    public function index(){
        echo "I'm inside the index method of Birthday Class"."<br>";
    }

}