<?php
namespace App\City;

use App\Model\Database as DB;


class City extends DB{
    public $id;
    public $user_name;
    public $user_city;

    public function __construct()
    {
        parent::__construct();
    }

    public function index(){
        echo "I'm inside the index method of City Class"."<br>";
    }

}