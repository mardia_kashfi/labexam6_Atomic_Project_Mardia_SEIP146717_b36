<?php
namespace App\ProfilePicture;

use App\Model\Database as DB;


class ProfilePicture extends DB{
    public $id;
    public $user_name;
    public $profile_picture;

    public function __construct()
    {
        parent::__construct();
    }

    public function index(){
        echo "I'm inside the index method of Profile Picture Class"."<br>";
    }

}