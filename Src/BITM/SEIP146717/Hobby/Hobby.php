<?php
namespace App\Hobby;

use App\Model\Database as DB;


class Hobby extends DB{
    public $id;
    public $user_name;
    public $user_hobbies;

    public function __construct()
    {
        parent::__construct();
    }

    public function index(){
        echo "I'm inside the index method of Hobby Class"."<br>";
    }

}