<?php
namespace App\Gender;

use App\Model\Database as DB;


class Gender extends DB{
    public $id;
    public $user_name;
    public $gender;

    public function __construct()
    {
        parent::__construct();
    }

    public function index(){
        echo "I'm inside the index method of Gender Class"."<br>";
    }

}