<?php
namespace App\BookTitle;

use App\Message\Message;
use App\Model\Database as DB;
use App\Utility\Utility;


class BookTitle extends DB{
    public $id;
    public $book_title;
    public $author_name;

    public function __construct()
    {
        parent::__construct();
    }

    public function index(){
        echo "I'm inside the index method of BookTitle Class"."<br>";
    }// end of __construct ()

    public function  setData ($postVariableData=NULL){
        if(array_key_exists("id",$postVariableData)){
             $this->id=   $postVariableData['id'];
        }

        if(array_key_exists("book_title",$postVariableData)){
            $this->book_title=   $postVariableData['book_title'];
        }


        if(array_key_exists("author_name",$postVariableData)){
            $this->author_name=   $postVariableData['author_name'];
        }
    }// end of setData()


    public function  store(){
        $arrData = array($this->book_title,$this->author_name);

        $sql ="insert into book_title (book_title,author_name) VALUES (?,?)";

        $STH=  $this->DBH->prepare($sql);
        $result=$STH->execute($arrData);

if ($result)
        Message::message("Data has been inserted successfully :) ");

else

    Message::message("Failed!! Data has not been inserted successfully :( ");

        Utility::redirect("create.php");


    }

}//end of BookTitle Class